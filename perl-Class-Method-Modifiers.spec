Name:           perl-Class-Method-Modifiers
Version:        2.15
Release:        2
Summary:        Provides Moose-like method modifiers
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/Class-Method-Modifiers
Source0:        https://cpan.metacpan.org/authors/id/E/ET/ETHER/Class-Method-Modifiers-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:  coreutils findutils make perl-interpreter perl(Test::Requires) perl(Test::Fatal)
BuildRequires:  perl-generators perl(ExtUtils::MakeMaker) perl(Test::More) >= 0.88

Requires:       perl(B) perl(Carp) perl(Exporter)

%{?perl_default_filter}

%description
Method modifiers are a powerful feature from the CLOS (Common Lisp Object
System) world.

'Class::Method::Modifiers' provides three modifiers: 'before', 'around',
and 'after'. 'before' and 'after' are run just before and after the method
they modify, but can not really affect that original method. 'around' is
run in place of the original method, with a hook to easily call that
original method. See the 'MODIFIERS' section for more details on how the
particular modifiers work.

%package_help

%prep
%setup -q -n Class-Method-Modifiers-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} -c %{buildroot}

%check
make test

%files
%license LICENSE
%doc Changes CONTRIBUTING README t/
%{perl_vendorlib}/Class/

%files help
%{_mandir}/man3/Class::Method::Modifiers.3*

%changelog
* Tue Jan 14 2025 Funda Wang <fundawang@yeah.net> - 2.15-2
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Jul 10 2023 leeffo <liweiganga@uniontech.com> - 2.15-1
- upgrade to version 2.15

* Wed Jan 8 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.12-12
- Package init
